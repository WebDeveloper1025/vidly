﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Vidly.Models;
using Vidly.ViewModels;

namespace Vidly.Controllers
{
    public class MoviesController : Controller
    {
        // GET: Movies
        public ActionResult Random()
        {
            var movie = new Movie() { Name = "Shrek!" };

            /*ViewData["Movie"] = movie;*/
            //ViewBag.Movie = movie;

            var customers = new List<Customer>
            {
                new Customer { Name = "Customer1"},
                new Customer { Name = "Customer2"}
            };

            var ViewModel = new RandomMovieViewModel
            {
                Movie = movie,
                Customers = customers
            };

            return View(ViewModel);
            //return HttpNotFound();

        }
        public ActionResult Edit(int id)
        {
            
            return Content("id=" + id);

        }
        public ActionResult Index(int pageIndex, string sortBy)
        {
            if (pageIndex < 0)
                pageIndex = 1;

            if (String.IsNullOrWhiteSpace(sortBy))
                sortBy = "Name";

            return Content(String.Format("pageIndex={0}&soryBy={1}", pageIndex, sortBy));

        }

        [Route("movies/released/{year}/{month:regex(\\d{4}):range(1, 12)}")]
        public ActionResult ByReleaseYear(int year, int month)
        {
            return Content(year + "sdsdf/" + month);
        }
        public ActionResult ByReleaseDate(int year, int month)
        {
            return Content(year + "/" + month);  
        }
    }
}